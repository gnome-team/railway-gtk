railway-gtk (2.7.3-1) unstable; urgency=medium

  * New upstream release (Closes: #1098620)
  * Refresh patches for new upstream; stop patching deps
  * d/control: Update build-dependencies with debcargo

 -- Matthias Geiger <werdahias@debian.org>  Sun, 23 Feb 2025 16:08:25 +0100

railway-gtk (2.7.1-2) unstable; urgency=medium

  * Stop patching env-logger dependency

 -- Matthias Geiger <werdahias@debian.org>  Wed, 01 Jan 2025 21:46:20 +0100

railway-gtk (2.7.1-1) unstable; urgency=medium

  * New upstream release
  * Drop d/p/gtk-rs-0.9.patch, applied upstream
  * d/control, d/copyright: Update my mail address
  * d/p/disable-cargo-home-meson-build.diff: Refresh for new upstream
  * d/p/set-name-railway-gtk: Refresh fro new upstream
  * d/control: Build-Depend on blueprint-compiler

 -- Matthias Geiger <werdahias@debian.org>  Sat, 28 Dec 2024 21:47:43 +0100

railway-gtk (2.7.0-1) unstable; urgency=medium

  * New upstream release (Closes: #1075775)
  * Drop gtk-rs 0.8 patch; obsoloted
  * Drop patch fixing FTBFS in i386; upstreamed
  * relax-deps.diff: refresh for new upstream
  * CARGO_HOME patch: rebase for new upstream
  * d/control: Updated dependencies for new upstream release
  * d/control: Build with gtk-rs 0.9 (Closes: #1081894)
  * Add upstream patch for gtk-rs 0.9
  * d/p/project-name-railway-gtk.patch: Refresh for new upstream release

 -- Matthias Geiger <werdahias@debian.org>  Sat, 28 Dec 2024 00:51:04 +0100

railway-gtk (2.4.0-4) unstable; urgency=medium

  * Team upload
  * Update Homepage
  * Release to unstable

 -- Jeremy Bícha <jbicha@ubuntu.com>  Sun, 05 May 2024 09:02:40 -0400

railway-gtk (2.4.0-3) experimental; urgency=medium

  * Cherry-pick upstream patch for gtk-rs 0.8 
  * Updated build dependencies for gtk-rs 0.8
  * Bump Standards-Version to 4.7.0; no changes needed
  * Mention GNOME Circle in package description
  * Add Static-Built-Using and X-Cargo-Built-Using fields in d/control
  * Updated d/watch to reflect Gitlab API changes

 -- Matthias Geiger <werdahias@riseup.net>  Sat, 20 Apr 2024 15:28:19 +0200

railway-gtk (2.4.0-2) unstable; urgency=medium

  * Team upload
  * Cherry-pick patch to fix 32-bit build (Closes: #1066866)

 -- Jeremy Bícha <jbicha@ubuntu.com>  Thu, 04 Apr 2024 18:53:47 -0400

railway-gtk (2.4.0-1) unstable; urgency=medium

  * New upstream release
  * d/patches: update for new upstream version
  * d/control: update build dependencies for new upstream version
  * d/gbp.conf: add default commit message

 -- Arnaud Ferraris <aferraris@debian.org>  Mon, 04 Mar 2024 14:13:51 +0100

railway-gtk (2.2.0-1) unstable; urgency=medium

  * New upstream release
  * d/patches: refresh for new upstream version

 -- Arnaud Ferraris <aferraris@debian.org>  Tue, 02 Jan 2024 09:03:45 +0100

railway-gtk (2.1.0-1) unstable; urgency=medium

  [ Matthias Geiger ]
  * Initial release. (Closes: #1052683)

 -- Arnaud Ferraris <aferraris@debian.org>  Thu, 16 Nov 2023 17:50:59 +0100
