# Railway

[![Matrix](https://img.shields.io/badge/Matrix-Join-brightgreen)](https://matrix.to/#/%23railwayapp:matrix.org)
[![Translation status](https://hosted.weblate.org/widgets/schmiddi-on-mobile/-/railway/svg-badge.svg)](https://hosted.weblate.org/engage/schmiddi-on-mobile/)

Railway lets you look up travel information for many different railways, all without needing to navigate through different websites.

## Screenshots

![](/data/screenshots/overview.png)

## Installation

<table>
  <tr>
    <td>Flatpak</td>
    <td>
      <a href='https://flathub.org/apps/details/de.schmidhuberj.DieBahn'><img width='130' alt='Download on Flathub' src='https://flathub.org/api/badge?svg&locale=en'/></a>
    </td>
  </tr>
  <tr>
    <td>Arch Linux</td>
    <td>[railway](https://archlinux.org/packages/extra/x86_64/railway)</td>
  </tr>
  <tr>
    <td>Debian (testing)</td>
    <td>[railway-gtk](https://packages.debian.org/testing/railway-gtk)</td>
  </tr>
  <tr>
    <td>Debian (unstable)</td>
    <td>[railway-gtk](https://packages.debian.org/unstable/railway-gtk)</td>
  </tr>
</table>

## Features

- Search for journeys
- View the details of a journey including departure, arrivals, delays, platforms
- Adaptive for small screens
- Bookmark a search or a journey
- Show more information like prices
- Many different search profiles, e.g
    - DB
    - ÖBB
    - BART
    - ... (many more). For a full list of supported profiles, see [hafas-rs](https://gitlab.com/schmiddi-on-mobile/hafas-rs#profiles) which is used to query the application data.

## Translation

Railway can easily be translated for other languages, as it uses gettext. Please consider contributing translations using [Weblate](https://hosted.weblate.org/engage/schmiddi-on-mobile/), as an alternative you can also open merge requests and I will notify you if updates are necessary. Thanks to Weblate for free hosting and all the translators for their great work keeping the translations up-to-date.

<a href="https://hosted.weblate.org/engage/schmiddi-on-mobile/">
<img src="https://hosted.weblate.org/widgets/schmiddi-on-mobile/-/railway/multi-auto.svg" alt="Translation status" />
</a>

## Code of Conduct

This project follows [GNOME's Code of Conduct](https://conduct.gnome.org/).
